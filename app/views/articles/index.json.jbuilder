json.array!(@articles) do |article|
  json.extract! article, :title, :description, :price
  json.url article_url(article, format: :json)
end
