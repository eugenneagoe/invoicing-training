json.array!(@orders) do |order|
  json.extract! order, :client_name, :address
  json.url order_url(order, format: :json)
end
