class OrderMailer < ActionMailer::Base
  default from: "eugen.neagoe@appmospheres.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.notify.subject
  #
  def notify(order)
    @order = Order.find(order)
    mail to: order.email, subject: 'Your order'
  end

end
