class CreateOrderLines < ActiveRecord::Migration
  def change
    create_table :order_lines do |t|
      t.references :article, index: true
      t.references :order, index: true
      t.decimal :quantity

      t.timestamps
    end
  end
end
